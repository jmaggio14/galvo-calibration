This script can be used to drive and measure the position of a galvanometer.
This is to provide ground truth position data for calibrating the DPI or DDPI -
especially at frequencies >10Hz where the galvo stuggles to make rapid changes
without significant overshoot.


### TO USE

 1. Make sure you are on a computer with an NI-compatible DAQ and the MATLAB
 data acquisition toolbox
 2. Double check the variables at the top of `galvo_collect.m` and make sure
 they are correct for your setup. In particularly the `DEVICE` variable - which
 can be checked using `daq.getDevices`
 3. Hit run
 4. Click on the matlab figure and hit `ESC` when you want to end the trial


### Data Output


data is saved in a csv file called `<YOUR SESSION NAME HERE>.csv`. it is column
delimited as follows:  _timestamps, input driver signal, galvo output signal_
