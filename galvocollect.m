
DEVICE = 'Dev1'; % can be found with daq.getDevices()
DRIVER_CHANNEL = 'ai1';
GAVLO_READ = 'ai0'; 
DRIVER_TRIGGER = 'ao0'; % must be analog output 
SAMPLE_RATE = 50e3; % samples/second
OSCILLATION_FREQUENCY = 1; %HZ
AMPLITUDE = 3; % Volts
SESSION_NAME = input('what would you like to call this session?\n', 's');
CALLBACK_LIMIT = round(SAMPLE_RATE,0); % samples 

% open the file for this session
fid = fopen([SESSION_NAME,'.csv'], 'w');
fprintf(fid,'timestamps,driver_signal,galvo\n');


s = daq.createSession('ni');
s.Rate = SAMPLE_RATE;
s.NotifyWhenDataAvailableExceeds = CALLBACK_LIMIT;



%%%%%%%% INPUT %%%%%%%%%%%%
[c1, ch1] = addAnalogInputChannel(s,DEVICE,DRIVER_CHANNEL,'Voltage');
[c2, ch2] = addAnalogInputChannel(s,DEVICE,GAVLO_READ,'Voltage');


lh_save = s.addlistener('DataAvailable', ...
              @(src,event) savedata(src,event,fid, ch1,ch2));
lh_plot = s.addlistener('DataAvailable', ...
              @(src,event) plotdata(src,event,ch1,ch2));
          
          

%%%%%%%% OUTPUT %%%%%%%%%%%%
s.addAnalogOutputChannel(DEVICE, DRIVER_TRIGGER, 'Voltage')
% generate a OSCILLATION_FREQUENCY Hz square wave to push into the galvo
% driver
trigger_wave = square( 2*pi*OSCILLATION_FREQUENCY .* linspace(0, 1, s.Rate) );
trigger_wave = (trigger_wave + 1) * (AMPLITUDE / 2);
trigger_wave = reshape(trigger_wave,numel(trigger_wave),1);



% create a handler to drive the external galvo driver
s.queueOutputData(trigger_wave);
lh_trigger = addlistener(s,'DataRequired', ...
			@(src,event) src.queueOutputData(trigger_wave));
        
        
disp('DAQ configured and setup...');
s.IsContinuous = 1;
figure;
hold on;
s.startBackground();


while true
    w = waitforbuttonpress;
    if w
        key = get(gcf,'currentcharacter');
        if key == 27 % ESCAPE KEY
            break;
        end
    end
end
s.stop();
daq.reset()
fclose(fid);
close;



