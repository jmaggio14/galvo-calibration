function [] = plotdata(src,event,ch1,ch2)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
subplot(1,1,1);
clf();
hold on;
plot(event.TimeStamps,event.Data(:,ch1),'r');
plot(event.TimeStamps,event.Data(:,ch2),'g');
% axis([event.TimeStamps(1), event.TimeStamps(end), 0, 11]);
xlabel('Time (seconds)');
ylabel('Voltage (V)');
legend('input signal','uncal position');
end

